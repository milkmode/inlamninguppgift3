package prog;

import robocode.*;

import java.awt.*;

public class MiniRobot extends AdvancedRobot {

    @Override
    public void run() {
        setBodyColor(Color.darkGray);
        setRadarColor(Color.red);
        setScanColor(Color.yellow);
        setGunColor(Color.green);
        //move to wider space
        double movement = Math.max(getBattleFieldWidth(), getBattleFieldHeight());
        ahead(movement);

        while (true) {
            ahead(movement);
            turnGunRight(360);
            back(100);
            turnGunRight(360);

        }
    }

    @Override
    public void onScannedRobot(ScannedRobotEvent event) {
        double robotEnergy = getEnergy();
        double scannedDistance = event.getDistance();

        if (scannedDistance > 500 && scannedDistance < 600 && robotEnergy > 100)
            fire(1);
        else if (scannedDistance > 400 && scannedDistance <= 500 && robotEnergy > 100)
            fire(2);
        else if (scannedDistance > 200 && scannedDistance <= 400)
            fire(2);
        else if (scannedDistance < 200)//use more fire power when close
            fire(3);

    }

    @Override
    public void onHitWall(HitWallEvent event) {
        double bearing = event.getBearing(); //get the bearing of the wall
        //go opposite of the wall
        turnRight(-bearing);
        ahead(100); //The robot goes away from the wall.
    }

    @Override
    public void onHitRobot(HitRobotEvent event) {
        //if close to other robot

        if (event.getBearing() > -100 && event.getBearing() < 100) {
            back(100);
            scan();
        } else
            ahead(100);
    }

    @Override
    public void onHitByBullet(HitByBulletEvent event) {
        double bearing = event.getBearing(); //Get the direction of arriving bullet

        if (getEnergy() < 150) { // if energy low
            turnRight(-bearing);
            ahead(100); //goes away from the opponent

        } else
            turnRight(360);

    }
}
